# S0-Logger
The S0-Logger consists of 2 Boards
- 230V to 12V Powersupply
- Controllerboard (nrf52840)

## Controller Board
![S0-Logger Controller board](doc/S0-logger-controller-render-3d-Rev-2.png)

## Powersupply
![S0-Logger Powersupply board](doc/S0-logger-render-3d-Rev-2.png)

# License
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under the <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
