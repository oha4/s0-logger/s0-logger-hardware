# Measurement Setup
![Measurement Setup](output_voltage_ripple_setup.jpg)

# Output Voltage Ripple over Load:

![Open Load](output_voltage_ripple_open_load.BMP)  
Load: None, Vpp: 136mV, Freq: 2,12kHz


![Load 109 Ohm](output_voltage_ripple_109ohm.BMP)  
Load: 109Ohm, Vpp: 144mV, Freq: 35kHz

![Load 231 Ohm](output_voltage_ripple_231ohm.BMP)  
Load: 231Ohm, Vpp: 138mV, freq: 17,86kHz

# Output Voltage over Load

| Load    | Output Voltage |
|---------|----------------|
| None    | 12,3V          |
| 231 Ohm | 11,51          |
| 109 Ohm | 11,25          |

